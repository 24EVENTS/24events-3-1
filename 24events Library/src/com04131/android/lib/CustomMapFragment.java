package com04131.android.lib;

import android.os.Bundle;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * User: Goddchen
 * Date: 13.02.13
 */
public class CustomMapFragment extends SupportMapFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getMap() != null) {
            getMap().getUiSettings().setMyLocationButtonEnabled(true);
            getMap().getUiSettings().setRotateGesturesEnabled(false);
            getMap().getUiSettings().setScrollGesturesEnabled(true);
            getMap().getUiSettings().setTiltGesturesEnabled(false);
            getMap().getUiSettings().setZoomControlsEnabled(true);
            getMap().getUiSettings().setZoomGesturesEnabled(true);
            getMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
            getMap().setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    //TODO show detail page
                }
            });
            new Thread(getAndAddMarkers).start();
        }
    }

    private Runnable getAndAddMarkers = new Runnable() {
        @Override
        public void run() {
            final List<Event> events = Helper.getEvents(getActivity());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (Event event : events) {
                        getMap().addMarker(new MarkerOptions()
                                .position(new LatLng(0, 0))
                                .draggable(false)
                                .snippet(event.venue)
                                .title(event.title));
                    }
                }
            });
        }
    };

}
