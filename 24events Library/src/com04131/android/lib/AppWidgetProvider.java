package com04131.android.lib;

import java.util.Arrays;
import java.util.List;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com04131.android.lib.Helper.INTENT_ACTIONS;

public class AppWidgetProvider extends android.appwidget.AppWidgetProvider {

	// public static List<Event> events;
	// public static int currentPosition = 0;

	// public static final String ACTION_BACK = "04131.ACTION_BACK";
	// public static final String ACTION_FORWARD = "04131.ACTION_FORWARD";

	public static final int REQUEST_BACK = 1;
	public static final int REQUEST_FORWARD = 2;
	public static final int REQUEST_ICON = 3;
	public static final int REQUEST_TITLE = 4;
	public static final int REQUEST_CONTENT = 5;
	public static final int REQUEST_REFRESH = 6;

	public static final String PREF_DATA = "data";
	public static final String PREF_POSITION = "position";

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		Log.d(context.getPackageName(), "onUpdate("
				+ Arrays.toString(appWidgetIds) + ")");
		Helper.updateWidget(context, false);
//		IntentFilter filter = new IntentFilter();
//		filter.addAction(Helper.getIntentAction(context, INTENT_ACTIONS.BACK));
//		filter.addAction(Helper
//				.getIntentAction(context, INTENT_ACTIONS.FORWARD));
//		context.getApplicationContext().registerReceiver(this, filter);
		// for (int i = 0; i < appWidgetIds.length; i++) {
		// RemoteViews views = new RemoteViews(context.getPackageName(),
		// R.layout.widget);
		// views.setOnClickPendingIntent(R.id.back, PendingIntent
		// .getBroadcast(context, REQUEST_BACK,
		// new Intent(ACTION_BACK), 0));
		// views.setOnClickPendingIntent(R.id.forward, PendingIntent
		// .getBroadcast(context, REQUEST_FORWARD, new Intent(
		// ACTION_FORWARD), 0));
		// views.setOnClickPendingIntent(R.id.content, PendingIntent
		// .getActivity(context, REQUEST_CONTENT, new Intent(context,
		// MainActivity.class), 0));
		// views
		// .setOnClickPendingIntent(
		// R.id.widget_title,
		// PendingIntent
		// .getActivity(
		// context,
		// REQUEST_TITLE,
		// new Intent(
		// Intent.ACTION_VIEW,
		// Uri
		// .parse(context
		// .getString(R.string.url_widget_title))),
		// 0));
		// views.setOnClickPendingIntent(R.id.widget_icon, PendingIntent
		// .getActivity(context, REQUEST_ICON, new Intent(context,
		// MainActivity.class), 0));
		// appWidgetManager.updateAppWidget(appWidgetIds[i], buildRemoteViews(
		// context, false));
		// }
		context.startService(new Intent(context, UpdateWidgetService.class));
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		Log.d(context.getPackageName(), "Action: " + intent.getAction());
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		// AppWidgetManager appWidgetManager = AppWidgetManager
		// .getInstance(context);
		try {
			int currentPosition = prefs.getInt(AppWidgetProvider.PREF_POSITION,
					0);
			// if (ACTION_BACK.equals(intent.getAction())) {
			if (intent.getAction().equals(
					Helper.getIntentAction(context, INTENT_ACTIONS.BACK))) {
				Log.d(context.getPackageName(), "BACK");
				currentPosition = Math.max(0, currentPosition - 1);
				prefs.edit().putInt(AppWidgetProvider.PREF_POSITION,
						currentPosition).commit();
				Helper.updateWidget(context, false);
				// ComponentName componentName = new ComponentName(context,
				// AppWidgetProvider.class);
				// appWidgetManager.updateAppWidget(componentName,
				// buildRemoteViews(
				// context, false));
				// } else if (ACTION_FORWARD.equals(intent.getAction())) {
			} else if (intent.getAction().equals(
					Helper.getIntentAction(context, INTENT_ACTIONS.FORWARD))) {
				Log.d(context.getPackageName(), "FORWARD");
				// List<Event> events = Helper.stringToEvents(context, prefs
				// .getString(AppWidgetProvider.PREF_DATA, null));
				List<Event> events = Helper.loadEvents(context);
				if (events != null) {
					currentPosition = Math.min(currentPosition + 1, events
							.size() - 1);
				} else {
					currentPosition = 0;
				}
				prefs.edit().putInt(AppWidgetProvider.PREF_POSITION,
						currentPosition).commit();
				Helper.updateWidget(context, false);
				// ComponentName componentName = new ComponentName(context,
				// AppWidgetProvider.class);
				// appWidgetManager.updateAppWidget(componentName,
				// buildRemoteViews(
				// context, false));
			}
		} catch (Exception e) {
			Log.e(context.getPackageName(), "Error handling intent", e);
		}
	}

	// public static RemoteViews buildRemoteViews(Context context, boolean
	// loading) {
	// RemoteViews views = new RemoteViews(context.getPackageName(),
	// R.layout.widget);
	// if (events != null && currentPosition < events.size()) {
	// views.setTextViewText(R.id.location,
	// events.get(currentPosition).location);
	// views
	// .setTextViewText(R.id.title,
	// events.get(currentPosition).title);
	// GregorianCalendar cal = new GregorianCalendar();
	// cal.setTime(events.get(currentPosition).startDate);
	// views.setTextViewText(R.id.start_date, String
	// .format("%02d.%02d.", cal.get(Calendar.DAY_OF_MONTH), cal
	// .get(Calendar.MONTH) + 1));
	// cal.setTime(events.get(currentPosition).startTime);
	// views.setTextViewText(R.id.start_time, String.format("%02d:%02d",
	// cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
	// views.setBoolean(R.id.back_wrapper, "setEnabled",
	// currentPosition > 0);
	// views.setBoolean(R.id.forward_wrapper, "setEnabled",
	// currentPosition < events.size() - 1);
	// }
	//
	// if (loading) {
	// views.setViewVisibility(R.id.content, View.GONE);
	// views.setViewVisibility(R.id.loading, View.VISIBLE);
	// views.setViewVisibility(R.id.refresh_wrapper, View.GONE);
	// } else if (events != null) {
	// views.setViewVisibility(R.id.content, View.VISIBLE);
	// views.setViewVisibility(R.id.loading, View.GONE);
	// views.setViewVisibility(R.id.refresh_wrapper, View.GONE);
	// } else {
	// views.setViewVisibility(R.id.content, View.GONE);
	// views.setViewVisibility(R.id.refresh_wrapper, View.VISIBLE);
	// views.setViewVisibility(R.id.loading, View.GONE);
	// }
	//
	// views.setOnClickPendingIntent(R.id.back_wrapper,
	// PendingIntent.getBroadcast(context, REQUEST_BACK, new Intent(
	// ACTION_BACK), 0));
	// views.setOnClickPendingIntent(R.id.forward_wrapper, PendingIntent
	// .getBroadcast(context, REQUEST_FORWARD, new Intent(
	// ACTION_FORWARD), 0));
	// Intent contentIntent = new Intent(context, MainActivity.class);
	// if (events != null && currentPosition < events.size()) {
	// Log.d(context.getPackageName(), "Setting url2 on pending intent");
	// contentIntent.putExtra("event", events.get(currentPosition));
	// }
	// views.setOnClickPendingIntent(R.id.content, PendingIntent.getActivity(
	// context, REQUEST_CONTENT, contentIntent,
	// PendingIntent.FLAG_UPDATE_CURRENT));
	// views.setOnClickPendingIntent(R.id.widget_title, PendingIntent
	// .getActivity(context, REQUEST_TITLE, new Intent(
	// Intent.ACTION_VIEW, Uri.parse(context
	// .getString(R.string.url_widget_title))), 0));
	// views.setOnClickPendingIntent(R.id.widget_icon, PendingIntent
	// .getActivity(context, REQUEST_ICON, new Intent(context,
	// MainActivity.class), 0));
	// views.setOnClickPendingIntent(R.id.refresh, PendingIntent.getService(
	// context, REQUEST_REFRESH, new Intent(context,
	// UpdateWidgetService.class), 0));
	// return views;
	// }

}
