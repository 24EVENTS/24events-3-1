package com04131.android.lib.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com04131.android.lib.R;

/**
 * User: Goddchen
 * Date: 18.02.13
 */
public class RemoveAdsNoticeDialogFragment extends SherlockDialogFragment {

    public static RemoveAdsNoticeDialogFragment newInstance() {
        return new RemoveAdsNoticeDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.dialog_remove_ads_message))
                .setTitle(getString(R.string.dialog_remove_ads_title)).setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RemoveAdsIAPDialogFragment.newInstance().show(getFragmentManager(), "iap");
            }
        }).setNegativeButton(android.R.string.cancel, null).create();
    }
}
