package com04131.android.lib;

/**
 * User: Goddchen
 * Date: 20.02.13
 */
public class Billing {

    public static final String TAG = "Billing";
    public static final String IAB_REMOVE_ADS_LEVEL_1 = "remove.ads.1";
    public static final String IAB_REMOVE_ADS_LEVEL_2 = "remove.ads.2";
    public static final String IAB_REMOVE_ADS_LEVEL_3 = "remove.ads.3";
    public static final String IAB_REMOVE_ADS_LEVEL_4 = "remove.ads.4";
    public static final String IAB_REMOVE_ADS_LEVEL_5 = "remove.ads.5";
    public static final String IAB_REMOVE_ADS_LEVEL_6 = "remove.ads.6";

}
