package com04131.android.lib;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.codeslap.persistence.Persistence;
import com.codeslap.persistence.SqlAdapter;
import com04131.android.lib.data.MerklisteItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebViewFragment extends SherlockFragment implements OnClickListener {

    private boolean mInitialLoad = true;

    public WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.webview, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (webView == null || webView.getUrl() == null || !webView.getUrl().contains("view=details&id=")) {
            menu.removeItem(R.id.merkliste_add);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.merkliste_add) {
            try {
                Pattern pattern = Pattern.compile("view=details&id=(\\d*?)[%&]", Pattern.DOTALL);
                String url = webView.getUrl();
                Matcher matcher = pattern.matcher(url);
                if (matcher.find()) {
                    long id = Long.parseLong(matcher.group(1));
                    SqlAdapter adapter = Persistence.getAdapter(getActivity());
                    if (adapter.count(MerklisteItem.class, "_id=?", new String[]{"" + id}) == 0) {
                        MerklisteItem merklisteItem = new MerklisteItem();
                        merklisteItem.id = id;
                        merklisteItem.title = webView.getTitle().substring(webView.getTitle().indexOf("-") + 2);
                        merklisteItem.created = System.currentTimeMillis();
                        Persistence.getAdapter(getActivity()).store(merklisteItem);
                        Toast.makeText(getActivity(), getString(R.string.add_to_merkliste), Toast.LENGTH_SHORT).show();
                        getLoaderManager().restartLoader(MainActionBarActivity.LOADER_MERKLISTE, null,
                                ((MainActionBarActivity) getActivity()).mMerklisteFragment.mMerklisteLoaderCallbacks);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.already_on_merkliste),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                Log.e(WebViewFragment.class.getSimpleName(), "Error adding Merkliste item", e);
                Toast.makeText(getActivity(), "Fehler beim Hinzufügen zur Merkliste!", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.webview, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.retry).setOnClickListener(this);
        webView = new WebView(getActivity()) {
            @Override
            public void loadUrl(String url) {
                IABActivity iabActivity = (IABActivity) getActivity();
                if (!url.contains("&esnesda=true") && iabActivity.inAppBillingAvailable && iabActivity.inventory !=
                        null && (iabActivity.inventory.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_1) || iabActivity
                        .inventory.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_2) || iabActivity.inventory.hasPurchase
                        (Billing.IAB_REMOVE_ADS_LEVEL_3) || iabActivity.inventory.hasPurchase(Billing
                        .IAB_REMOVE_ADS_LEVEL_4) || iabActivity.inventory.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_5)
                        || iabActivity.inventory.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_6))) {
                    super.loadUrl(url + "&esnesda=true");
                } else {
                    super.loadUrl(url);
                }
            }
        };
        webView.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        ((ViewGroup) view.findViewById(R.id.webview)).addView(webView);
        if (getArguments().getString("url").contains("twitter.com")) {
            webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) " +
                    "AppleWebKit/530.17 (KHTML," + " like Gecko) Version/4.0 Mobile Safari/530.17");
        }
        final View loading = view.findViewById(R.id.loading);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + " 04131 App");
        WebChromeClient webChromeClient = new WebChromeClient() {
        };
        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(getClass().getSimpleName(), "Loading url: " + url);
                if (url.contains("maps.google.de")) {
                    Intent mapsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0," +
                            "0?q=" + Uri.parse(url).getQueryParameter("q")));
                    startActivity(Intent.createChooser(mapsIntent, getString(R.string.open_map)));
                    return true;
                } else if (Uri.parse(url).getHost().equals("bit.ly")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, url);
                    startActivity(Intent.createChooser(intent, getString(R.string.share_event)));
                    return true;
                } else if (!Uri.parse(url).getHost().contains("events.org") && !Uri.parse(url).getHost().contains
                        ("04131.com") && !Uri.parse(url).getHost().contains("twitter.com")) {
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, Uri.parse(url)),
                            getString(R.string.open_with)));
                    return true;
                } else {
                    webView.loadUrl(url);
                    return true;
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loading.setVisibility(View.GONE);
                if (url.equals(getArguments().getString("url")) && getArguments().containsKey("url2") && mInitialLoad) {
                    mInitialLoad = false;
                    view.loadUrl(getArguments().getString("url2"));
                }
                if (url.endsWith("error.html")) {
                    getView().findViewById(R.id.retry).setVisibility(View.VISIBLE);
                }
                SherlockFragmentActivity sherlockActivity = getSherlockActivity();
                if (sherlockActivity != null) {
                    sherlockActivity.supportInvalidateOptionsMenu();
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loading.setVisibility(View.VISIBLE);
                if (getView() != null && getView().findViewById(R.id.retry) != null) {
                    getView().findViewById(R.id.retry).setVisibility(View.GONE);
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                view.loadUrl("file:///android_asset/error.html");
            }
        };
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(webViewClient);
        // if (getIntent().getExtras() != null) {
        // for (String key : getIntent().getExtras().keySet()) {
        // Log.d(getPackageName(), "Extra: key=" + key + " value="
        // + getIntent().getExtras().get(key));
        // }
        // }
        // webView.loadUrl(getIntent().getStringExtra("url"));
        webView.loadUrl(getArguments().getString("url"));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.retry) {
            webView.loadUrl(getArguments().getString("url"));
        }
    }

}
