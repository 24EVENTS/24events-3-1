package com04131.android.lib;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com04131.android.lib.fragments.dialogs.RemoveAdsNoticeDialogFragment;

import java.util.Random;

/**
 * User: Goddchen
 * Date: 16.02.13
 */
public class IABActivity extends SherlockFragmentActivity implements IabHelper.OnIabSetupFinishedListener,
        IabHelper.OnIabPurchaseFinishedListener, IabHelper.QueryInventoryFinishedListener {

    public IabHelper iabHelper;

    public boolean inAppBillingAvailable = false;

    public Inventory inventory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getString(R.string.iab_public_key).length() > 0) {
            iabHelper = new IabHelper(this, getString(R.string.iab_public_key));
            iabHelper.enableDebugLogging(BuildConfig.DEBUG, Billing.TAG);
            iabHelper.startSetup(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            iabHelper.dispose();
        } catch (Exception e) {
            Log.w(IABActivity.class.getSimpleName(), "Error disposing iab helper", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        inAppBillingAvailable = result.isSuccess();
        if (result.isSuccess()) {
            iabHelper.queryInventoryAsync(this);
        }
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (result.isFailure()) {
            Toast.makeText(this, getString(R.string.toast_iap_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, getString(R.string.toast_iap_success), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
        inventory = inv;
        if (!inv.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_1) && !inv.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_2) &&
                !inv.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_3) && !inv.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_4)
                && !inv.hasPurchase(Billing.IAB_REMOVE_ADS_LEVEL_5) && !inv.hasPurchase(Billing
                .IAB_REMOVE_ADS_LEVEL_6) && new Random().nextInt() % 10 == 0) {
            RemoveAdsNoticeDialogFragment.newInstance().show(getSupportFragmentManager(), "iap-notice");
        }
    }
}
