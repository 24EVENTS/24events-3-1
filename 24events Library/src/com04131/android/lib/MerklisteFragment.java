package com04131.android.lib;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.codeslap.persistence.Persistence;
import com.codeslap.persistence.SqlAdapter;
import com04131.android.lib.adapter.MerklisteItemAdapter;
import com04131.android.lib.async.MerklisteLoader;
import com04131.android.lib.data.MerklisteItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Goddchen
 * Date: 16.04.13
 */
public class MerklisteFragment extends ListFragment implements ActionMode.Callback {

    public static MerklisteFragment newInstance() {
        MerklisteFragment fragment = new MerklisteFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().restartLoader(MainActionBarActivity.LOADER_MERKLISTE, null, mMerklisteLoaderCallbacks);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_merkliste, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setBackgroundColor(Color.parseColor("#ffffff"));
        getListView().setDivider(new ColorDrawable(Color.parseColor("#eeeeee")));
        getListView().setDividerHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
                getResources().getDisplayMetrics()));
        getListView().setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (((MainActionBarActivity) getActivity()).actionMode == null) {
                    getListView().setItemChecked(position, true);
                    ((MainActionBarActivity) getActivity()).actionMode =
                            ((SherlockFragmentActivity) getActivity()).startActionMode(MerklisteFragment.this);
                }
                return true;
            }
        });
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (((MainActionBarActivity) getActivity()).actionMode == null) {
                    MerklisteItem item = (MerklisteItem) getListAdapter().getItem(position);
                    ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
                    WebViewFragment fragment = ((MainActionBarActivity) getActivity()).mAllFragment;
                    fragment.webView.loadUrl(getString(R.string.url_all) + "&view=details&id=" + item.id);
                    viewPager.setCurrentItem(1, true);
                } else {
                    if (getSelectedItems().size() == 0) {
                        ((MainActionBarActivity) getActivity()).actionMode.finish();
                    }
                    ((ArrayAdapter) getListAdapter()).notifyDataSetChanged();
                }
            }
        });
    }

    private List<MerklisteItem> getSelectedItems() {
        List<MerklisteItem> items = new ArrayList<MerklisteItem>();
        for (int i = 0; i < getListView().getCount(); i++) {
            if (getListView().isItemChecked(i)) {
                items.add((MerklisteItem) getListAdapter().getItem(i));
            }
        }
        return items;
    }

    public LoaderManager.LoaderCallbacks<List<MerklisteItem>> mMerklisteLoaderCallbacks = new LoaderManager
            .LoaderCallbacks<List<MerklisteItem>>() {

        @Override
        public Loader<List<MerklisteItem>> onCreateLoader(int i, Bundle bundle) {
            return new MerklisteLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<MerklisteItem>> listLoader, List<MerklisteItem>
                merklisteItems) {
            if (merklisteItems != null) {
                setListAdapter(new MerklisteItemAdapter(getActivity(), merklisteItems));
            }
        }

        @Override
        public void onLoaderReset(Loader<List<MerklisteItem>> listLoader) {

        }
    };

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        ((SherlockFragmentActivity) getActivity()).getSupportMenuInflater().inflate(R.menu.actionmode_merkliste, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (item.getItemId() == R.id.delete) {
            SqlAdapter adapter = Persistence.getAdapter(getActivity());
            for (MerklisteItem merklisteItem : getSelectedItems()) {
                adapter.delete(merklisteItem);
            }
            getLoaderManager().restartLoader(MainActionBarActivity.LOADER_MERKLISTE, null, mMerklisteLoaderCallbacks);
            mode.finish();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        ((MainActionBarActivity) getActivity()).actionMode = null;
        for (int i = 0; i < getListView().getCount(); i++) {
            getListView().setItemChecked(i, false);
        }
    }
}
