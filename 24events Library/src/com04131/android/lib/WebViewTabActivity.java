package com04131.android.lib;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewTabActivity extends Activity {

	private WebView mWebView;

	private boolean mInitialLoad = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		mWebView.getSettings().setUserAgentString(
				mWebView.getSettings().getUserAgentString() + " 04131 App");
		WebChromeClient webChromeClient = new WebChromeClient() {
		};
		WebViewClient webViewClient = new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.d(getPackageName(), "Loading url: " + url);
				if (url.contains("maps.google.de")) {
					Intent mapsIntent = new Intent(Intent.ACTION_VIEW,
							Uri.parse("geo:0,0?q="
									+ Uri.parse(url).getQueryParameter("q")));
					startActivity(Intent.createChooser(mapsIntent,
							getString(R.string.open_map)));
					return true;
				} else if (!Uri.parse(url).getHost().contains("events.org")
						&& !Uri.parse(url).getHost().contains("04131.com")) {
					startActivity(Intent.createChooser(new Intent(
							Intent.ACTION_VIEW, Uri.parse(url)),
							getString(R.string.open_with)));
					return true;
				} else {
					return false;
				}
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				findViewById(R.id.loading).setVisibility(View.GONE);
				if (url.equals(getIntent().getStringExtra("url"))
						&& getIntent().hasExtra("url2") && mInitialLoad) {
					mInitialLoad = false;
					view.loadUrl(getIntent().getStringExtra("url2"));
				}
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				mWebView.loadUrl("file:///android_asset/error.html");
			}
		};
		mWebView.setWebChromeClient(webChromeClient);
		mWebView.setWebViewClient(webViewClient);
		if (getIntent().getExtras() != null) {
			for (String key : getIntent().getExtras().keySet()) {
				Log.d(getPackageName(), "Extra: key=" + key + " value="
						+ getIntent().getExtras().get(key));
			}
		}
		mWebView.loadUrl(getIntent().getStringExtra("url"));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN
				&& keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {
			mWebView.goBack();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

}
