package com.maler.dieck.android.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.maler.dieck.android.R;
import com.maler.dieck.android.fragments.ImageFragment;

public class ImageActivity extends SherlockFragmentActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fragment_activity);
		Fragment fragment = new ImageFragment();
		fragment.setArguments(getIntent().getExtras());
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment, fragment).commit();
	}

}
