package com.maler.dieck.android.fragments;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.maler.dieck.android.R;
import com.maler.dieck.android.activities.ImageActivity;

public class GalleryFragment extends SherlockFragment implements
		OnItemClickListener {

	private LruCache<String, Bitmap> mCache;

	@Override
	public void onDestroy() {
		super.onDestroy();
		mCache.evictAll();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCache = new ImageCache();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.gallery, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		GridView gridView = (GridView) view.findViewById(R.id.grid);
		gridView.setAdapter(new GridAdapter());
		gridView.setOnItemClickListener(this);
	}

	private List<String> getImages() {
		try {
			return Arrays.asList(getResources().getAssets().list("gallery"));
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), "Error loading images", e);
			return Collections.emptyList();
		}
	}

	private class GridAdapter extends ArrayAdapter<String> {
		public GridAdapter() {
			super(getActivity(), android.R.layout.simple_list_item_1,
					getImages());
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final String file = getItem(position);
			final View v;
			if (convertView == null) {
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.item_grid, parent, false);
			} else {
				v = convertView;
			}
			final ImageView imageView = (ImageView) v.findViewById(R.id.image);
			new Thread(new Runnable() {

				@Override
				public void run() {
					final Bitmap bitmap = mCache.get(file);
					imageView.post(new Runnable() {

						@Override
						public void run() {
							imageView.setImageBitmap(bitmap);
						}
					});
				}
			}).start();
			return v;
		}
	}

	private class ImageCache extends LruCache<String, Bitmap> {

		public ImageCache() {
			super(6 * 1024 * 1024);
		}

		@Override
		protected Bitmap create(String key) {
			try {
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(
						getResources().getAssets().open("gallery/" + key),
						null, options);
				int sampleSize = 1;
				while (options.outWidth / sampleSize > 200) {
					sampleSize *= 2;
				}
				options.inJustDecodeBounds = false;
				options.inSampleSize = sampleSize;
				return BitmapFactory.decodeStream(getResources().getAssets()
						.open("gallery/" + key), null, options);
			} catch (Exception e) {
				Log.e(getClass().getSimpleName(), "Error loading image", e);
				return BitmapFactory.decodeResource(getResources(),
						android.R.drawable.ic_menu_gallery);
			}
		}

		@Override
		protected void entryRemoved(boolean evicted, String key,
				Bitmap oldValue, Bitmap newValue) {
			super.entryRemoved(evicted, key, oldValue, newValue);
			if (evicted) {
				oldValue.recycle();
			}
		}

		@Override
		protected int sizeOf(String key, Bitmap value) {
			return value.getRowBytes() * value.getHeight();
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		startActivity(new Intent(getActivity().getApplicationContext(),
				ImageActivity.class).putExtra("file",
				(String) ((GridView) getView().findViewById(R.id.grid))
						.getAdapter().getItem(arg2)));
	}
}
